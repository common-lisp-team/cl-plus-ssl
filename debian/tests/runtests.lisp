(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "cl+ssl.test"))

(let ((results (5am:run :cl+ssl)))
  (5am:explain! results)
  (unless (5am:results-status results)
    (uiop:quit 1)))
